# GIT REP
Tenha acesso aos repositórios públicos do GitHub e suas informações

## Introdução
O aplicativo Git Rep traz consigo uma maneira fácil e ágil de acessar repositórios públicos de um determinado usuário. Com ele, você tem acesso à uma lista contendo os repositórios em ordem de popularidade, e ao acessar os detalhes, informações como descrição, linguagem, data de criação, forks, stars e issues abertas e fechadas são dispostas para visualização. Também acesse diretamente o repositório pelo browser com o direcionamento através da tela dos detalhes.

## Motivação
A criação do aplicativo teve como objetivo a conclusão do desafio proposto pela empresa Radix, para que seja suprido um dos tópicos necessários do processo seletivo.

## Interface da Aplicação

![alt text](https://firebasestorage.googleapis.com/v0/b/gitrep-259bc.appspot.com/o/Design%20sem%20nome%20(3).png?alt=media&token=a42e5f6d-b99a-4e57-a672-94d9d884bb36)

## Como Utilizar
1. Na aba principal, insira um usuário do GitHub válido;
2. Após ser direcionado para a aba dos repositórios, clique em um repositório de sua preferência;
3. O aplicativo acessará os dados do repositório escolhido e mostrará-los na tela.
4. Caso queira acessar o repositório pelo browser, clique no botão "Abrir no GitHub" que você será redirecionado.

## Confecção e Tecnologias Utilizadas
O aplicativo em sua totalidade foi construído em linguagem Java, pela IDE Android Studio. O Git Rep utilizou componentes nativos do sistema Android, API do GitHub para a integração de dados propostos pelo seu objetivo, e bibliotecas necessárias para o tratamento e disposição dos dados. 

Para as HTTP requests, foi utilizada a biblioteca Retrofit 2 a fim de se obter os dados JSON e JSONArray da API do GitHub. Com ela, foram utilizados o Gson e Scalars Converter também do Retrofit 2 para a conversão em Object e String, respectivamente. Para a conversão das Strings JSON para Object, foi utilizado o JSONObject faz a conversão completa da String e permite o acesso de seus elementos. A ListView foi preenchida a partir da lista de objetos gerada com o Retrofit 2 e Gson Converter. 

Quanto ao avatar do usuário, foi utilizada a biblioteca Picasso com o intuito de carregar a imagem em formato URL e assim armazená-lo em cache na memória e no disco rígido do dispositivo. A imagem é carregada e exposta com margem circular através da biblioteca CicrleImageView.
A tela final de descrição do repositório dispõe de um CardView com uma Intent que redireciona o conteúdo para o browser e acesso ao repositório no próprio site do GitHub.

A orientação de tela sem perda de estado foi utilizada declarando nas activities do Manifest o android:configChanges="orientation|screenSize|keyboardHidden".

O .gitignore foi criado com base no gitignore.io para o commit dos repositórios e arquivos necessários.

Sobre o teste de interface, foi utilizado a ferramenta Espresso e testado em todos os componentes da aplicação. Quanto aos testes unitários, o JUnit 4 foi utilizado para o teste de resposta das requisições HTTP efetuadas pelo Retrofit 2.

Ambos os erros que possam surgir foram tratados, seja quanto aos erros da requisição HTTP, seja dos componentes do aplicativo manupulados.

## Autor

*  Fabricio Ramos da Costa (http://gitlab.com/ramosaegir)

