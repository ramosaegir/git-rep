package ramosaegir.gitrep.models;

public class GitHubIssuesClosed {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
