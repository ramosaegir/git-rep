package ramosaegir.gitrep.models;

public class GitHubUser {
    String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
