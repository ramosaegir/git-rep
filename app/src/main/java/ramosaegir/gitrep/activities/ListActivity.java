package ramosaegir.gitrep.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ramosaegir.gitrep.API.Client;
import ramosaegir.gitrep.API.ServiceReposList;
import ramosaegir.gitrep.API.ServiceUser;
import ramosaegir.gitrep.R;
import ramosaegir.gitrep.adapters.GitHubAdapter;
import ramosaegir.gitrep.models.GitHubRepos;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity {


    private TextView mUsername_txt;
    private ListView listView;
    private AlertDialog alert;
    private ProgressBar progressBar;
    private ImageView avatar_img;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mUsername_txt = (TextView) findViewById(R.id.username_txt);
        listView = (ListView) findViewById(R.id.listview);
        progressBar = (ProgressBar) findViewById(R.id.pb_main);
        avatar_img = (ImageView) findViewById(R.id.avatar);
        toolbar = (Toolbar) findViewById(R.id.toolbar_list);

        Intent i = getIntent();
        final String user_name = i.getStringExtra("USER_NAME");

        getToolbarBackButton(toolbar);

        collapsingToolbarConfig();

        progressBar.setVisibility(View.VISIBLE);

        requestRetrofitToList(user_name);

        requestRetrofitNameAndAvatar(user_name);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView txt_name = (TextView) view.findViewById(R.id.titlerep_txt);
                String name = txt_name.getText().toString();
                Intent i = new Intent(ListActivity.this, DetailActivity.class);
                List<String> values = new ArrayList<>();
                values.add(user_name);
                values.add(name);
                i.putStringArrayListExtra("REPOS", (ArrayList<String>) values);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getToolbarBackButton(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void collapsingToolbarConfig(){
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Repositórios");
                    collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    public void alertDialog(String title, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent i = new Intent(ListActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
        alert = builder.create();
        alert.show();
    }

    public void requestRetrofitToList(String user_name){
        ServiceReposList serviceReposList = Client.retrofitBuilder_Converter().create(ServiceReposList.class);
        Call<List<GitHubRepos>> callAsync = serviceReposList.getRepository(user_name);
        callAsync.enqueue(new Callback<List<GitHubRepos>>() {
            @Override
            public void onResponse(Call<List<GitHubRepos>> call, Response<List<GitHubRepos>> response) {
                if (response.isSuccessful()) {
                    List<GitHubRepos> results = response.body();
                    GitHubAdapter adapter = new GitHubAdapter(getApplicationContext(), 0, results);
                    progressBar.setVisibility(View.GONE);
                    listView.setAdapter(adapter);
                } else {
                    switch (response.code()) {
                        case 404:
                            alertDialog("Usuário não Encontrado",
                                    "Verifique o usuário inserido e tente novamente.");
                            break;
                        case 500:
                            alertDialog("Problemas com o Servidor",
                                    "Falha ao conectar com o servidor." +
                                            " Tente novamente mais tarde.");
                            break;
                        default:
                            alertDialog("Erro Desconhecido",
                                    "Tente novamente mais tarde.");
                            break;
                    }
                }
            }
            @Override
            public void onFailure(Call<List<GitHubRepos>> call, Throwable t) {
                alertDialog("Falha na Conexão",
                        "Verifique sua conexão e tente novamente.");
            }
        });
    }

    public void requestRetrofitNameAndAvatar(String user_name){
        ServiceUser serviceUser = Client.retrofitBuilder_Scalars().create(ServiceUser.class);
        Call<String> stringCall = serviceUser.getUserInfos(user_name);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String responseString = response.body();
                    fetchingData(responseString);
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    public void fetchingData(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            String u = obj.getString("login");
            String avatar = obj.getString("avatar_url");
            mUsername_txt.setText(u);
            Picasso.get().load(avatar).error(R.drawable.ic_error).into(avatar_img);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}