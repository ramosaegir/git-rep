package ramosaegir.gitrep.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ramosaegir.gitrep.API.Client;
import ramosaegir.gitrep.API.ServiceIssuesClosed;
import ramosaegir.gitrep.API.ServiceRepos;
import ramosaegir.gitrep.R;
import ramosaegir.gitrep.models.GitHubIssuesClosed;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;

public class DetailActivity extends AppCompatActivity {

    private TextView mLang_txt, mDesc_txt, mStar_txt, mFork_txt, mData_txt, mIssuesOpen_txt, mIssuesClosed_txt;
    private String link;
    private CardView cardView_git;
    private Toolbar toolbar;
    private ArrayList<String> values_array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mLang_txt = (TextView) findViewById(R.id.lang_txt);
        mDesc_txt = (TextView) findViewById(R.id.desc_txt);
        mStar_txt = (TextView) findViewById(R.id.star_txt);
        mFork_txt = (TextView) findViewById(R.id.fork_txt);
        mData_txt = (TextView) findViewById(R.id.data_txt);
        mIssuesOpen_txt = (TextView) findViewById(R.id.issues_open_txt);
        mIssuesClosed_txt = (TextView) findViewById(R.id.issues_closed_txt);
        cardView_git = (CardView) findViewById(R.id.cardview_git);
        toolbar = (Toolbar) findViewById(R.id.toolbar_detail);

        Intent i = getIntent();
        values_array = i.getStringArrayListExtra("REPOS");

        getToolbarBackButton(toolbar);
        getSupportActionBar().setTitle(values_array.get(1));

        requestRetrofitInfos(values_array.get(0), values_array.get(1));

        requestRetrofitClosedIssues(values_array.get(0), values_array.get(1));

        cardView_git.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                startActivity(browserIntent);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void getToolbarBackButton(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void alert(String s){
        Toast.makeText(this,s,LENGTH_SHORT).show();
    }

    public void requestRetrofitInfos(String user_name, String name_repo){
        ServiceRepos serviceRepos = Client.retrofitBuilder_Scalars().create(ServiceRepos.class);
        Call<String> stringCall = serviceRepos.getRepository(user_name, name_repo);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String responseString = response.body();
                    fetchingData(responseString);
                }else{
                    switch (response.code()) {
                        case 404:
                            alert("Usuário não Encontrado.\nVerifique o usuário inserido e tente novamente.");
                            break;
                        case 500:
                            alert("Falha ao conectar com o servidor.\nTente novamente mais tarde.");
                            break;
                        default:
                            alert("Erro Desconhecido.\nTente novamente mais tarde.");
                            break;
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                alert("Falha na conexão com o servidor.\nVerifique sua conexão e tente novamente.");
            }
        });
    }

    public void requestRetrofitClosedIssues(String user_name, String name_repo){
        ServiceIssuesClosed serviceIssuesClosed = Client.retrofitBuilder_Converter().create(ServiceIssuesClosed.class);
        Call<List<GitHubIssuesClosed>> callIssues = serviceIssuesClosed.getIssuesClosed(user_name, name_repo);
        callIssues.enqueue(new Callback<List<GitHubIssuesClosed>>() {
            @Override
            public void onResponse(Call<List<GitHubIssuesClosed>> call, Response<List<GitHubIssuesClosed>> response) {
                if(response.isSuccessful()){
                    List<GitHubIssuesClosed> results = response.body();
                    int itemCount = results.size();
                    if(itemCount <= 1){
                        mIssuesClosed_txt.setText(itemCount + " fechada");
                    }else{
                        mIssuesClosed_txt.setText(itemCount + " fechadas");
                    }
                }else{
                    alert("Falha ao obter resposta do servidor.");
                }
            }
            @Override
            public void onFailure(Call<List<GitHubIssuesClosed>> call, Throwable t) {
                alert("Ocorreu um erro.\nTente novamente mais tarde.");
            }
        });
    }

    public void fetchingData(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            String stars = obj.getString("stargazers_count");
            String forks = obj.getString("forks_count");
            String data_criacao = obj.getString("created_at");
            String descricao = obj.getString("description");
            String linguagem = obj.getString("language");
            int open_issue = obj.getInt("open_issues_count");
            link = obj.getString("html_url");
            mStar_txt.setText(stars);
            mFork_txt.setText(forks);
            if(descricao == "null"){
                mDesc_txt.setText("Sem descrição disponível para o repositório.");
            }else{
                mDesc_txt.setText(descricao);
            }
            if(linguagem == "null"){
                mLang_txt.setText("Sem linguagem definida.");
            }else{
                mLang_txt.setText(linguagem);
            }
            if(open_issue <= 1){
                mIssuesOpen_txt.setText("Issues: " + open_issue + " aberta / ");
            }else{
                mIssuesOpen_txt.setText("Issues: " + open_issue + " abertas / ");
            }

            parseDataGit(data_criacao);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void parseDataGit(String data_criacao){
        String time = data_criacao.substring(0, data_criacao.length() - 10);
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
            mData_txt.setText(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
