package ramosaegir.gitrep.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import ramosaegir.gitrep.API.Client;
import ramosaegir.gitrep.API.ServiceUser;
import ramosaegir.gitrep.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {

    private EditText mSearch_edit;
    private Button mSearch_btn;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearch_edit = (EditText) findViewById(R.id.search_edit);
        mSearch_btn = (Button) findViewById(R.id.search_btn);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mSearch_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = mSearch_edit.getText().toString();
                if(username.isEmpty()) {
                    alert("Insira o nome do usuário para iniciar a consulta.");
                }else if(username.length() < 4){
                    alert("Caracteres insuficientes para buscar o usuário.\n" +
                            "Informe corretamente o nome do usuário para prosseguir.");
                }else{
                    mProgressBar.setVisibility(View.VISIBLE);
                    requestRetrofit(username);
                }
            }
        });

    }

    public void requestRetrofit(final String username) {

        ServiceUser service = Client.retrofitBuilder_Scalars().create(ServiceUser.class);
        Call<String> stringCall = service.getUserInfos(username);
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Intent i = new Intent(MainActivity.this, ListActivity.class);
                    i.putExtra("USER_NAME", username);
                    startActivity(i);
                    mProgressBar.setVisibility(View.GONE);
                }else{
                    switch (response.code()) {
                        case 404:
                            alert("Usuário não Encontrado.\n" +
                                    "Verifique o usuário inserido e tente novamente.");
                            break;
                        case 500:
                            alert("Falha ao conectar com o servidor.\n" +
                                    "Tente novamente mais tarde.");
                            break;
                        default:
                            alert("Erro Desconhecido.\nTente novamente mais tarde.");
                            break;
                    }
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                alert("Falha na conexão com o servidor.\n" +
                        "Verifique sua conexão e tente novamente.");
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    public void alert(String s){
        Toast.makeText(this,s,LENGTH_SHORT).show();
    }

}
