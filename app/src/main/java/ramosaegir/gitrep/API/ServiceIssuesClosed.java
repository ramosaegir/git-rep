package ramosaegir.gitrep.API;

import java.util.List;

import ramosaegir.gitrep.models.GitHubIssuesClosed;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceIssuesClosed {

    @GET("repos/{user}/{name}/issues?state=closed")
    Call<List<GitHubIssuesClosed>> getIssuesClosed (@Path("user") String user, @Path("name") String rep);
}
