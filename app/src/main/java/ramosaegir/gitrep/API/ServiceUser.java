package ramosaegir.gitrep.API;

import java.util.List;

import ramosaegir.gitrep.models.GitHubRepos;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceUser {

    @GET("users/{user}")
    Call<String> getUserInfos(@Path("user") String user);
}
