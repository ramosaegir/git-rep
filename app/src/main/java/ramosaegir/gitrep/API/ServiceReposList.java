package ramosaegir.gitrep.API;

import java.util.List;

import ramosaegir.gitrep.models.GitHubRepos;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceReposList {
    @GET("users/{user}/repos?sort=stars+order=desc")
    Call<List<GitHubRepos>> getRepository(@Path("user") String repos);
}