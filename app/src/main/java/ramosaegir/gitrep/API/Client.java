package ramosaegir.gitrep.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Client {

    static String URL_BASE = "http://api.github.com/";

    public static Retrofit retrofitBuilder_Converter(){
        Retrofit builder = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return builder;
    }

    public static Retrofit retrofitBuilder_Scalars(){
        Retrofit builder = new Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .baseUrl(URL_BASE)
                .build();
        return builder;
    }



}
