package ramosaegir.gitrep.API;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceRepos {

    @GET("repos/{user}/{name}")
    Call<String> getRepository(@Path("user") String user, @Path("name") String rep);

}
