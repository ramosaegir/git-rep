package ramosaegir.gitrep.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ramosaegir.gitrep.R;
import ramosaegir.gitrep.models.GitHubRepos;

public class GitHubAdapter extends ArrayAdapter<GitHubRepos> {

    private Context context;
    private List<GitHubRepos> data;

    public GitHubAdapter(@NonNull Context context, int resource, @NonNull List<GitHubRepos> data) {
        super(context, resource, data);

        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        GitHubRepos gu = data.get(position);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.list_rep_row, parent, false);

        TextView txtViewName = (TextView) convertView.findViewById(R.id.titlerep_txt);
        TextView txtViewDesc = (TextView) convertView.findViewById(R.id.descrep_txt);
        ImageView forward_img = (ImageView) convertView.findViewById(R.id.forward_img);
        txtViewName.setText(gu.getName());
        if (gu.getDescription() == null){
            txtViewDesc.setText("Sem descrição disponível.");
        }else{
            txtViewDesc.setText(gu.getDescription());
        }
        forward_img.setBackgroundResource(R.drawable.ic_forward);
        forward_img.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorPrimary));

        return convertView;
    }
}
