package ramosaegir.gitrep;

import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import ramosaegir.gitrep.API.Client;
import ramosaegir.gitrep.API.ServiceIssuesClosed;
import ramosaegir.gitrep.API.ServiceRepos;
import ramosaegir.gitrep.API.ServiceReposList;
import ramosaegir.gitrep.API.ServiceUser;
import ramosaegir.gitrep.activities.ListActivity;
import ramosaegir.gitrep.activities.MainActivity;
import ramosaegir.gitrep.adapters.GitHubAdapter;
import ramosaegir.gitrep.models.GitHubIssuesClosed;
import ramosaegir.gitrep.models.GitHubRepos;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(AndroidJUnit4.class)
public class TestUnits {

    // Teste de Resposta da API para o ServiceUser
    @Test
    public void retrofitRequestServiceUser() throws IOException {
        String username = "";
        ServiceUser service = Client.retrofitBuilder_Scalars().create(ServiceUser.class);
        Call<String> stringCall = service.getUserInfos(username);
        Assert.assertNotNull(stringCall.execute());
    }

    @Test
    public void retrofitRequestServiceRepos() throws IOException {
        String user_name = "";
        String name_repo = "";
        ServiceRepos serviceRepos = Client.retrofitBuilder_Scalars().create(ServiceRepos.class);
        Call<String> stringCall = serviceRepos.getRepository(user_name, name_repo);
        Assert.assertNotNull(stringCall.execute());
    }

    // Teste de Resposta da API para o ServiceReposList
    @Test
    public void retrofitRequestServiceReposList() throws IOException {
        String username = "";
        ServiceReposList serviceReposList = Client.retrofitBuilder_Converter().create(ServiceReposList.class);
        Call<List<GitHubRepos>> callAsync = serviceReposList.getRepository(username);
        Assert.assertNotNull(callAsync.execute());
    }

    // Teste de Resposta da API para o ServiceIssuesClosed
    @Test
    public void retrofitRequestServiceIssuesClosed() throws IOException {
        String user_name = "";
        String name_repo = "";
        ServiceIssuesClosed serviceIssuesClosed = Client.retrofitBuilder_Converter().create(ServiceIssuesClosed.class);
        Call<List<GitHubIssuesClosed>> callIssues = serviceIssuesClosed.getIssuesClosed(user_name, name_repo);
        Assert.assertNotNull(callIssues.execute());
    }
}
